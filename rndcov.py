import numpy
import numpy.random
from rndhaar import rndhaar


def rndcov_diag(diagarr):
    """Create a random covariance matrix from eigenvalues on the diagonal."""
    d = numpy.asarray(diagarr)
    q = rndhaar(d.shape[0])
    r = numpy.dot(q * d, q.T)
    return 0.5 * (r + r.T)


def rndncov(n):
    """Create random covariance of dimension n using the Gramian of standard
    Gaussian random numbers.
    """
    x = numpy.random.randn(n, n)
    r = numpy.dot(x.T, x)
    return 0.5 * (r + r.T)
