import numpy
import scipy.linalg


def sqrt_by_eig(l, m):
    """Matrix square root by eigen-decomposition factors l, m, where l is the
    diagonal matrix of eigenvalues and m is the orthogonal matrix of
    eigenvectors.
    """
    s = numpy.diag(numpy.sqrt(l))
    r = numpy.dot(m, numpy.dot(s, m.T))
    return .5 * (r + r.T)


def cmpmatrix(ref, alt, fulloutput=False):
    """Matrix comparator FROM ref TO alt.
    If fulloutput, also return the square root of the inverse of ref."""
    refeigval, refeigvec = scipy.linalg.eigh(ref, check_finite=False)
    # Square root of ref.
    sref = sqrt_by_eig(refeigval, refeigvec)
    # Inverse of sqrt(ref)
    srefinv = sqrt_by_eig(1.0 / refeigval, refeigvec)
    # The "middle" term
    mid = numpy.dot(sref, numpy.dot(alt, sref))
    mid = .5 * (mid + mid.T)
    meigval, meigvec = scipy.linalg.eigh(mid, check_finite=False)
    sm = sqrt_by_eig(meigval, meigvec)
    r = numpy.dot(srefinv, numpy.dot(sm, srefinv))
    r = .5 * (r + r.T)
    if not fulloutput:
        return r
    else:
        return r, srefinv
