Please view the Jupyter notebook [`session.ipynb`][1] via nbviewer.

[1]: https://nbviewer.jupyter.org/urls/gitlab.com/congma/matrix_diagonality/raw/master/session.ipynb "NBViewer view of the notebook 'session.ipynb'"
