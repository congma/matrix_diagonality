import numpy
import numpy.random
import scipy.linalg


def rndhaar(n):
    """Generate a random (real) orthogonal matrix of dimension n distributed as
    the Haar measure on the orthogonal group.
    """
    z = numpy.random.randn(n, n)
    q, r = scipy.linalg.qr(z, overwrite_a=True, check_finite=False)
    l = numpy.sign(numpy.diag(r))
    return q * l
