import numpy
import scipy.linalg
from comparator import cmpmatrix, sqrt_by_eig
from rndhaar import rndhaar
from rndcov import rndcov_diag, rndncov


def shape_nonconform_p(arr):
    return not ((len(arr.shape) == 2) and (arr.shape[0] == arr.shape[1]))


def off_diag_ssq(matrix):
    """Off-diagonal sum of squares of a matrix.
    Returns the sum of squares (Frobenius norm squared), diagonal sum of
    squares, the difference (i.e. off-diagonal sum of squares), and the ratio
    of the diagonal sum of squares to the total sum.
    """
    sq = matrix * matrix
    a = sq.sum()
    b = numpy.diag(sq).sum()
    return a, b, a - b, b / a


class DiagonalConcentrationTester(object):
    """Tester class for the hypothesis."""

    def __init__(self, *args, **kwargs):
        """DiagonalConcentrationTester() initiates a default tester instance
        with random covariance matrices as the two comparable objects.
        DiagonalConcentrationTester(ref, alt) initiates the instance with the
        two given matrices as ref and alt respectively.
        """
        try:
            self.ref = args[0]
            self.alt = args[1]
        except IndexError:
            self.dimension = kwargs.get("N", 10)
            self.ref = rndncov(self.dimension)
            self.alt = rndncov(self.dimension)
        else:
            if ((self.ref.shape != self.alt.shape) or
                shape_nonconform_p(self.ref) or shape_nonconform_p(self.alt)):
                raise ValueError("Shape non-conformane in input matrices.")
            self.dimension = self.ref.shape[0]
        # The "baseline" matrix W-tilde
        self.stdw, self.srefinv = cmpmatrix(self.ref, self.alt,
                                            fulloutput=True)
        self.salt = sqrt_by_eig(*scipy.linalg.eigh(self.alt,
                                                   check_finite=False))
        _, __, self.stdw_diff, self.stdw_ratio = off_diag_ssq(self.stdw)
        self.msamples = []
        self.diff_samples = []
        self.ratio_samples = []

    def sample(self, n, keep_m=True):
        """Creat n samples of test matrices and store them in self.msamples.
        """
        for i in xrange(n):
            q = rndhaar(self.dimension)
            w = reduce(numpy.dot, (self.salt, q, self.srefinv))
            _, __, d, r = off_diag_ssq(w)
            if keep_m:
                self.msamples.append(w)
            self.diff_samples.append(d)
            self.ratio_samples.append(r)
